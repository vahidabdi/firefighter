defmodule Firefighter.Answer do
  use Firefighter.Web, :model

  schema "answers" do
    field :answer, :string
    field :answer_reply, :string
    field :point, :integer
    belongs_to :question, Firefighter.Question

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:answer, :answer_reply, :point, :question_id])
    |> validate_required([:answer, :answer_reply, :point, :question_id])
    |> foreign_key_constraint(:question_id)
  end
end
