defmodule Firefighter.Question do
  use Firefighter.Web, :model

  schema "questions" do
    field :question, :string
    field :question_no, :integer
    belongs_to :exam, Firefighter.Exam
    has_many :answers, Firefighter.Answer

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:question, :question_no, :exam_id])
    |> validate_required([:question, :question_no, :exam_id])
    |> unique_constraint(:question_no)
    |> foreign_key_constraint(:exam_id)
  end
end
