defmodule Firefighter.RefUser do
  use Firefighter.Web, :model

  schema "ref_users" do
    belongs_to :chat, Firefighter.Chat
    belongs_to :ref_chat, Firefighter.RefChat

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [])
    |> validate_required([])
  end
end
