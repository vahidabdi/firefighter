defmodule Firefighter.Content do
  use Firefighter.Web, :model

  schema "contents" do
    field :category, :string
    field :content, :string
    field :content_no, :integer

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:category, :content, :content_no])
    |> validate_required([:category, :content, :content_no])
  end
end
