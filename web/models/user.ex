defmodule Firefighter.User do
  use Firefighter.Web, :model

  alias Firefighter.Repo

  schema "users" do
    field :chat_id, :integer
    field :point, :integer
    field :state, :integer
    field :activate, :boolean
    field :content_state, :integer
    field :first_name, :string
    field :last_name, :string
    field :username, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:chat_id, :point, :first_name, :last_name, :username])
    |> validate_required([:chat_id])
    |> unique_constraint(:chat_id)
  end

  def activate_user(struct, params \\ %{}) do
    struct
    |> cast(params, [:chat_id, :activate])
    |> validate_required([:chat_id, :activate])
    |> unique_constraint(:chat_id)
  end
end
