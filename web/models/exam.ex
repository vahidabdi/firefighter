defmodule Firefighter.Exam do
  use Firefighter.Web, :model

  schema "exams" do
    field :name, :string
    has_many :questions, Firefighter.Question

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name)
  end
end
