defmodule Firefighter.QuestionController do
  use Firefighter.Web, :controller

  import Ecto.Query

  alias Firefighter.{Repo, Question}

  alias Firefighter.Exam
  def index(conn, _params) do
    questions = Repo.all(Question)
    render(conn, "index.html", questions: questions)
  end

  def new(conn, _params) do
    changeset = Question.changeset(%Question{})
    query = from(e in Exam, select: {e.name, e.id})
    exams = Repo.all(query)
    render(conn, "new.html", changeset: changeset, exams: exams)
  end

  def create(conn, %{"question" => question_params}) do
    changeset = Question.changeset(%Question{}, question_params)

    case Repo.insert(changeset) do
      {:ok, _question} ->
        conn
        |> redirect(to: question_path(conn, :index))
      {:error, changeset} ->
        query = from(e in Exam, select: {e.name, e.id})
        exams = Repo.all(query)
        render(conn, "new.html", changeset: changeset, exams: exams)
    end
  end

  def edit(conn, %{"id" => id}) do
    question = Repo.get!(Question, id)
    changeset = Question.changeset(question)
    query = from(e in Exam, select: {e.name, e.id})
    exams = Repo.all(query)
    render(conn, "edit.html", question: question, changeset: changeset, exams: exams)
  end

  def update(conn, %{"id" => id, "question" => question_params}) do
    question = Repo.get!(Question, id)
    changeset = Question.changeset(question, question_params)

    case Repo.update(changeset) do
      {:ok, _changeset} ->
        conn
        |> redirect(to: question_path(conn, :index))
      {:error, changeset} ->
        query = from(e in Exam, select: {e.name, e.id})
        exams = Repo.all(query)
        render(conn, "new.html", changeset: changeset, exams: exams)
    end
  end
end
