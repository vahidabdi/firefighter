defmodule Firefighter.ExamController do
  use Firefighter.Web, :controller

  alias Firefighter.Repo
  alias Firefighter.Exam

  def index(conn, _params) do
    exams = Repo.all(Exam) |> Repo.preload([:questions])
    render(conn, "index.html", exams: exams)
  end

  def new(conn, _params) do
    changeset = Exam.changeset(%Exam{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"exam" => exam_param}) do
    changeset = Exam.changeset(%Exam{}, exam_param)

    case Repo.insert(changeset) do
      {:ok, _changeset} ->
        conn
        |> redirect(to: exam_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset, action: :new)
    end
  end
end
