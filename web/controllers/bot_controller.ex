defmodule Firefighter.BotController do
  use Firefighter.Web, :controller

  alias Firefighter.Telegram

  require Logger

  def webhook(conn, %{"token" => token, "message" => message} = params) do
    Logger.info(token)
    apply(Telegram, :handle_message, [token, message])
    json conn, %{ok: "ok"}
  end

  def webhook(conn, %{"token" => token, "callback_query" => callback_query} = params) do
    Logger.info(token)
    apply(Telegram, :handle_callback, [token, callback_query])
    json conn, %{ok: "ok"}
  end
end
