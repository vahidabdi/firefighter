defmodule Firefighter.AnswerController do
  use Firefighter.Web, :controller

  import Ecto.Query

  alias Firefighter.{Repo, Answer, Question}

  def index(conn, _params) do
    answers = Repo.all(Answer)
    render(conn, "index.html", answers: answers)
  end

  def new(conn, _params) do
    changeset = Answer.changeset(%Answer{})
    query = from(q in Question, select: {q.question, q.id})
    questions = Repo.all(query)
    render(conn, "new.html", changeset: changeset, questions: questions)
  end

  def create(conn, %{"answer" => answer_params}) do
    changeset = Answer.changeset(%Answer{}, answer_params)

    case Repo.insert(changeset) do
      {:ok, _changeset} ->
        conn
        |> redirect(to: answer_path(conn, :index))
      {:error, changeset} ->
        query = from(q in Question, select: {q.question, q.id})
        questions = Repo.all(query)
        render(conn, "new.html", changeset: changeset, questions: questions)
    end
  end

  def edit(conn, %{"id" => id}) do
    answer = Repo.get!(Answer, id)
    changeset = Answer.changeset(answer)
    query = from(q in Question, select: {q.question, q.id})
    questions = Repo.all(query)
    render(conn, "edit.html", answer: answer, changeset: changeset, questions: questions)
  end

  def update(conn, %{"id" => id, "answer" => answer_params}) do
    answer = Repo.get!(Answer, id)
    changeset = Answer.changeset(answer, answer_params)

    case Repo.update(changeset) do
      {:ok, _changeset} ->
        conn
        |> redirect(to: answer_path(conn, :index))
     {:error, changeset} ->
        query = from(q in Question, select: {q.question, q.id})
        questions = Repo.all(query)
        render(conn, "new.html", changeset: changeset, questions: questions)
      end
  end
end
