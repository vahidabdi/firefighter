defmodule Firefighter.AnswerTest do
  use Firefighter.ModelCase

  alias Firefighter.Answer

  @valid_attrs %{answer: "some content", point: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Answer.changeset(%Answer{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Answer.changeset(%Answer{}, @invalid_attrs)
    refute changeset.valid?
  end
end
