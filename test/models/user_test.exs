defmodule Firefighter.UserTest do
  use Firefighter.ModelCase

  alias Firefighter.User

  @valid_attrs %{chat_id: 42, point: 42, state: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end
end
