defmodule Firefighter.ContentTest do
  use Firefighter.ModelCase

  alias Firefighter.Content

  @valid_attrs %{category: "some content", content: "some content", content_no: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Content.changeset(%Content{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Content.changeset(%Content{}, @invalid_attrs)
    refute changeset.valid?
  end
end
