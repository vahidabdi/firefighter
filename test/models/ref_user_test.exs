defmodule Firefighter.RefUserTest do
  use Firefighter.ModelCase

  alias Firefighter.RefUser

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = RefUser.changeset(%RefUser{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = RefUser.changeset(%RefUser{}, @invalid_attrs)
    refute changeset.valid?
  end
end
