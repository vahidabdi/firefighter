defmodule Firefighter.Repo.Migrations.CreateRefUser do
  use Ecto.Migration

  def change do
    create table(:ref_users) do
      add :chat_id, references(:users, on_delete: :nothing)
      add :ref_chat_id, references(:users, on_delete: :nothing)

      timestamps()
    end
    create index(:ref_users, [:chat_id])
    create unique_index(:ref_users, [:ref_chat_id])

  end
end
