defmodule Firefighter.Repo.Migrations.ModifyContent do
  use Ecto.Migration

  def change do
    alter table(:contents) do
      modify :content, :text
    end
  end
end
