defmodule Firefighter.Repo.Migrations.CreateContent do
  use Ecto.Migration

  def change do
    create table(:contents) do
      add :category, :string
      add :content, :string
      add :content_no, :integer

      timestamps()
    end

  end
end
