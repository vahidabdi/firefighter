defmodule Firefighter.Repo.Migrations.ModifyUserAddContentState do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :content_state, :integer, default: 1
    end

    create constraint(:users, :content_state_must_be_positive, check: "content_state >= 0")
  end
end
