defmodule Firefighter.Repo.Migrations.CreateExam do
  use Ecto.Migration

  def change do
    create table(:exams) do
      add :name, :string, null: false

      timestamps()
    end
    create unique_index(:exams, [:name])

  end
end
