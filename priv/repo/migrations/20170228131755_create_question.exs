defmodule Firefighter.Repo.Migrations.CreateQuestion do
  use Ecto.Migration

  def change do
    create table(:questions) do
      add :question, :string, null: false
      add :exam_id, references(:exams, on_delete: :delete_all)
      add :question_no, :integer, null: false

      timestamps()
    end
    create index(:questions, [:exam_id])
    create unique_index(:questions, :question_no)
    create constraint(:questions, :question_no_must_be_positive, check: "question_no > 0")
  end
end
