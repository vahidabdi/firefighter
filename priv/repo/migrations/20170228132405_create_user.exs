defmodule Firefighter.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :chat_id, :integer, null: false
      add :point, :integer, default: 0
      add :state, :integer, default: 1
      add :activate, :boolean, default: false

      timestamps()
    end
    create unique_index(:users, [:chat_id])
    create constraint(:users, :state_must_be_positive, check: "state >= 0")
    create constraint(:users, :point_must_be_positive, check: "point >= 0")

  end
end
