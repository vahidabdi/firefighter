defmodule Firefighter.Repo.Migrations.CreateAnswer do
  use Ecto.Migration

  def change do
    create table(:answers) do
      add :answer, :string, null: false
      add :answer_reply, :string, null: false
      add :point, :integer, null: false
      add :question_id, references(:questions, on_delete: :nothing)

      timestamps()
    end
    create index(:answers, [:question_id])

  end
end
