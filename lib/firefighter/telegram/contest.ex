defmodule Firefighter.Telegram.Contest do

  require Logger

  import Ecto.Query
  alias Firefighter.Telegram.API
  alias Firefighter.{Repo, RedisPool, User, Question, Answer}

  def get_question(chat_id) do
    today_tried = get_user_tried(chat_id)
    if today_tried > 5 do
      %{"text" => "شما هر پنج سوال امروز جواب دادی.", "reply_markup": API.back_to_menu()}
    else
      q = from(u in User,
               join: q in Question, on: u.state == q.question_no,
               left_join: a in Answer, on: a.question_id == q.id,
               where: u.chat_id == ^chat_id and u.activate == true,
               select: %{question: q.question, q_id: q.id, answer: a.answer, answer_id: a.id})
      res = Repo.all(q)
      case res do
        [] ->
          %{"text": "اتمام مسابقه", "reply_markup": API.back_to_menu()}
        _ ->
          question = Map.get(Enum.at(res, 0), :question)
          q_id = Map.get(Enum.at(res, 0), :q_id)
          answers = Enum.reduce(res, [], fn(x, list) ->
            List.insert_at(list, 0, %{answer: x.answer, id: x.answer_id})
          end)
          IO.puts(inspect answers)
          API.build_question(chat_id, question, q_id, answers)
      end

    end
  end

  def reply_answer(chat_id, chat_instance, q_id, answer_id) do
    incr_user_tried(chat_id)
    Logger.info(inspect chat_instance)
    Logger.info(inspect q_id)
    answer = get_answer(answer_id)
    u = get_user(chat_id)
    changeset = Ecto.Changeset.change(u,
                                      point: u.point + answer.point,
                                      state: u.state + 1)
    Logger.info("inspect user changeset before update #{inspect changeset}")
    case Repo.update(changeset) do
      {:ok, u} ->
        Logger.info("updated user: #{inspect u}")
        API.build_reply(answer.answer_reply, u.point)
      {:error, changeset} ->
        Logger.error("Error while updating user #{inspect changeset}")
    end
  end

  def get_answer(answer_id) do
    Repo.get(Answer, answer_id)
  end

  defp get_user(chat_id) do
    Repo.get_by!(User, %{chat_id: chat_id})
  end

  def get_user_tried(chat_id) do
    with {:ok, nil} <- RedisPool.command(~w(GET #{Date.utc_today}_#{chat_id})),
         {:ok, tried} <- RedisPool.command(~w(INCR #{Date.utc_today}_#{chat_id})) do
      tried
    else
      _ ->
        {:ok, tried} = RedisPool.command(~w(GET #{Date.utc_today}_#{chat_id}))
        Integer.parse(tried)
        |> elem(0)
    end
  end

  def incr_user_tried(chat_id) do
    RedisPool.command(~w(INCR #{Date.utc_today}_#{chat_id}))
  end
end
