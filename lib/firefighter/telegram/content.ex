defmodule Firefighter.Telegram.Content do

  require Logger

  import Ecto.Query
  alias Firefighter.Telegram.API
  alias Firefighter.{Repo, User, Content}

  def get_content(category, chat_id) do
    query = from(u in User,
             join: c in Content, on: u.content_state == c.content_no,
             where: u.chat_id == ^chat_id and u.activate == true and c.category == ^category,
             select: %{content: c.content, content_no: c.content_no})

    res = Repo.one(query)
    case res do
      nil ->
        %{"text": "محتوایی وجود ندارد", "reply_markup": API.back_to_menu()}
      _ ->
        Logger.info(res.content)
        API.build_content_menu(category, res.content)
    end
  end

  def get_next_content(category, chat_id) do
    u = Repo.get_by!(User, %{chat_id: chat_id})
    case Repo.get_by(Content, %{content_no: u.content_state + 1, category: category}) do
      nil ->
        %{"text": "ﻢﺤﺗﻭﺍیی ﻮﺟﻭﺩ ﻥﺩﺍﺭﺩ", "reply_markup": API.back_to_menu()}
      content ->
        changeset = Ecto.Changeset.change(
                      u,
                      content_state: u.content_state + 1)
        Repo.update(changeset)
        API.build_content_menu(category, content.content)
    end
  end

  def get_prev_content(category, chat_id) do
    u = Repo.get_by!(User, %{chat_id: chat_id})
    case Repo.get_by(Content, %{content_no: u.content_state - 1, category: category}) do
      nil ->
        %{"text": "ﻢﺤﺗﻭﺍیی ﻮﺟﻭﺩ ﻥﺩﺍﺭﺩ", "reply_markup": API.back_to_menu()}
      content ->
        changeset = Ecto.Changeset.change(
                      u,
                      content_state: u.content_state - 1)
        Repo.update(changeset)
        API.build_content_menu(category, content.content)
    end
  end
end
