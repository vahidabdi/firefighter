defmodule Firefighter.Telegram do
  require Logger

  alias Ecto.Multi
  alias Firefighter.Telegram.{API, Contest, LeaderBoard, Content}
  alias Firefighter.{Repo, RedisPool, User}

  @url "https://api.telegram.org/bot"
  @commands ["/start", "/help", "/stop"]
  @welcome_msg Application.get_env(:firefighter, :welcome_msg)
  @history_msg Application.get_env(:firefighter, :history_msg)
  @invite_msg Application.get_env(:firefighter, :invite_msg)
  @forward_invite_msg Application.get_env(:firefighter, :forward_invite_msg)
  @invite_others_msg Application.get_env(:firefighter, :invite_others_msg)
  @unsub_msg Application.get_env(:firefighter, :unsub_msg)

  def handle_message(token, %{"text" => text} = message) do
    %{"chat" => chat} = message
    chat_id = chat["id"]
    case String.starts_with?(text, @commands) do
      true ->
        m = handle_command(text, chat_id, token)
        send_result(m, token, "sendMessage", chat_id)
      false ->
        m = handle_text(text)
        send_result(m, token, "sendMessage", chat_id)
    end
  end

  def handle_callback(token, %{"data" => data_callback} = callback) do
    %{"from" => from} = callback
    %{"message" => message} = callback
    %{"chat_instance" => chat_instance} = callback
    message_id = message["message_id"]
    chat_id = from["id"]
    Logger.info(
      "data_callback:#{data_callback},from#{chat_id} message_id#{message_id}"
    )
    cond do
      "back_to_menu" == data_callback->
        m = get_main_menu()
        send_result(m, token, "editMessageText", chat_id, message_id)
      "activate" == data_callback ->
        activate_user(token, chat_id, message_id)
      "learning" == data_callback ->
        handle_learning(token, chat_id, message_id)
      "award" == data_callback ->
        handle_award(token, chat_id, message_id)
      "history_content" == data_callback ->
        handle_content(token, "history", chat_id, message_id)
      "4soori_content" == data_callback ->
        handle_content(token, "4soori", chat_id, message_id)
      "next_history_content" == data_callback ->
        handle_next_content(token, "history", chat_id, message_id)
      "next_4soori_content" == data_callback ->
        handle_next_content(token, "4soori", chat_id, message_id)
      "prev_history_content" == data_callback ->
        handle_prev_content(token, "history", chat_id, message_id)
      "prev_4soori_content" == data_callback ->
        handle_prev_content(token, "4soori", chat_id, message_id)
      "invite" == data_callback ->
        handle_invite(token, chat_id, message_id)
      "contest" == data_callback ->
        handle_contest(token, chat_id, message_id)
      "contest_mem" == data_callback ->
        handle_contest_mem(token, chat_id, message_id)
      "unsub" == data_callback ->
        handle_unsub(token, chat_id, message_id)
      %{"answer_id" => answer_id, "q_id" => q_id} = Regex.named_captures(~r/reply_(?<q_id>\d+)_(?<answer_id>\d+)/, data_callback) ->
        {q_id, _} = Integer.parse(q_id)
        {a_id, _} = Integer.parse(answer_id)
        handle_reply(token, chat_id, chat_instance, q_id, a_id, message_id)
    end
  end

  def get_chat(token, chat_id) do
    json = Poison.encode!(%{"chat_id": chat_id})
    url = url_for(token, "getChat")
    {:ok, %HTTPoison.Response{body: body}} = HTTPoison.post(url, json, [{"Content-Type", "application/json"}])
    Poison.decode!(body)
    |> get_user_info()
  end

  def get_user_info(json) do
    ["first_name", "last_name", "username"]
    |> Enum.map(fn x ->
      case json["result"][x] do
        nil -> Logger.info("#{x} not found from get_chat")
        res -> %{"#{x}": res}
      end
    end)
  end

  def send_result(result, token, method, chat_id) do
    url = url_for(token, method)
    request_body = Map.put(result, :chat_id, chat_id)
    {:ok, json} = Poison.encode(request_body)
    resp = HTTPoison.post(url, json, [{"Content-Type", "application/json"}])
    {:ok, %HTTPoison.Response{body: body}} = resp
    Logger.info(inspect json)
    Logger.info(inspect body)
  end

  def send_result(result, token, method, chat_id, m_id) do
    url = url_for(token, method)
    request_body = Map.put(result, :chat_id, chat_id)
    request_body = Map.put(request_body, :message_id, m_id)
    {:ok, json} = Poison.encode(request_body)
    resp = HTTPoison.post(url, json, [{"Content-Type", "application/json"}])
    {:ok, %HTTPoison.Response{body: body}} = resp
    Logger.info(inspect json)
    Logger.info(inspect body)
  end

  def send_result(result, token, method, chat_id, from_chat_id, m_id) do
    url = url_for(token, method)
    request_body = Map.put(result, :chat_id, chat_id)
    request_body = Map.put(request_body, :message_id, m_id)
    request_body = Map.put(request_body, :from_chat_id, from_chat_id)
    {:ok, json} = Poison.encode(request_body)
    resp = HTTPoison.post(url, json, [{"Content-Type", "application/json"}])
    {:ok, %HTTPoison.Response{body: body}} = resp
    Logger.info(inspect json)
    Logger.info(inspect body)
  end

  def url_for(token, method) do
    @url <> token <> "/" <> method
  end

  def handle_command(command, chat_id, token) do
    cond do
      String.starts_with?(command, "/start=") ->
        try do
          with [[_x, result]] <- Regex.scan(~r{^/start=([\S]+)}, command),
               decoded_chat_id <- :base64.decode(result),
               {referer, _}    <- Integer.parse(decoded_chat_id) do
                   handle_start(token, chat_id, referer)
          else
            :error ->
              :error
          end
        catch
          _, _ ->
           handle_start(token, chat_id)
        end
      String.starts_with?(command, "/start") ->
        handle_start(token, chat_id)
      String.starts_with?(command, "/help") ->
        handle_help(command)
      String.starts_with?(command, "/stop") ->
        handle_stop(command)
    end
  end

  def handle_text(text) do
    get_main_menu()
  end

  def handle_start(token, chat_id, referer) do
    add_reference(chat_id, referer)
    get_main_menu()
  end

  def handle_start(token, chat_id) do
    case add_user(token, chat_id) do
      {:ok, _} -> request_activation(chat_id)
      {:error, _} ->
        if user_is_active(chat_id) do
          get_main_menu()
        else
          request_activation(chat_id)
        end
    end
  end

  def add_reference(chat_id, referer) do
    # Multi.new
    # |> Multi.insert(:add_chat_id, User.changeset(%User{}, %{chat_id: chat_id})
    # |> Multi.insert(:add_reference,
  end

  def add_user(token, chat_id) do
    chat_info = get_chat(token, chat_id)
    res = Enum.reduce(chat_info, %{chat_id: chat_id}, fn (map, acc) -> Map.merge(acc, map) end)
    changeset = User.changeset(%User{}, res)
    res = Repo.insert(changeset)
    case res do
      {:ok, _changeset} -> Logger.info("inserted user")
      {:error, _changeset} -> Logger.error("user already in database")
    end
    res
  end

  def request_activation(chat_id) do
    menu = API.build_activate_menu()
    %{"text": @welcome_msg, "parse_mode": "Markdown", "reply_markup": menu}
  end

  def activate_user(token, chat_id, message_id) do
    user = Repo.get_by!(User, %{chat_id: chat_id})
    activation = User.activate_user(user, %{activate: true})
    Logger.info(inspect activation)
    Repo.update(activation)
    m = get_main_menu()
    send_result(m, token, "editMessageText", chat_id, message_id)
  end

  def handle_help(_command) do
    menu = API.build_main_menu()
    %{"text": "help", "parse_mode": "Markdown", "reply_markup": menu}
    Logger.info("help")
  end

  def handle_stop(_command) do
    menu = API.build_main_menu()
    %{"text": "stop", "parse_mode": "Markdown", "reply_markup": menu}
  end

  def handle_callback(callback) do
    handle_help(callback)
  end

  defp get_main_menu do
    menu = API.build_main_menu()
    %{"text": @welcome_msg,
      "parse_mode": "Markdown", "reply_markup": menu}
  end

  def handle_learning(token, chat_id, message_id) do
    m = API.back_to_menu()
    text = "آموزش آموزش آموزش"
    result = %{"text" => text, "reply_markup": m}
    send_result(result, token, "editMessageText", chat_id, message_id)
  end

  def handle_award(token, chat_id, message_id) do
    text = LeaderBoard.get_top_user(chat_id)
    send_result(text, token, "editMessageText", chat_id, message_id)
  end

  def handle_contest_mem(token, chat_id, message_id) do
    m = API.back_to_menu()
    result = %{"text": "در حال راه اندازی", "reply_markup": m}
    send_result(result, token, "editMessageText", chat_id, message_id)
  end

  def handle_content(token, category, chat_id, message_id) do
    m = API.back_to_menu()
    result = Content.get_content(category, chat_id)
    send_result(result, token, "editMessageText", chat_id, message_id)
  end

  def handle_next_content(token, category, chat_id, message_id) do
    m = API.back_to_menu()
    result = Content.get_next_content(category, chat_id)
    send_result(result, token, "editMessageText", chat_id, message_id)
  end

  def handle_prev_content(token, category, chat_id, message_id) do
    m = API.back_to_menu()
    result = Content.get_prev_content(category, chat_id)
    send_result(result, token, "editMessageText", chat_id, message_id)
  end

  def handle_invite(token, chat_id, message_id) do
    m = API.back_to_menu()
    result = %{"text" => @invite_msg}
    send_result(result, token, "editMessageText", chat_id, message_id)
    result = %{"text" => @invite_others_msg}
    send_result(result, token, "sendMessage", chat_id)
    result = %{"text" => @forward_invite_msg, "reply_markup": m}
    send_result(result, token, "sendMessage", chat_id)
  end

  def handle_contest(token, chat_id, message_id) do
    m = Contest.get_question(chat_id)
    send_result(m, token, "editMessageText", chat_id, message_id)
  end

  def handle_unsub(token, chat_id, message_id) do
    unsub_menu = [[%{"text": "😞 واقعا میخوای از مسابقه بیای بیرون", "url": "https://botshow.ir/"}]]
    m = API.back_to_menu()
    menu = Map.update(m, :inline_keyboard, m.inline_keyboard, &(&1 ++ unsub_menu))
    result = %{"text" => @unsub_msg, "reply_markup": menu}
    send_result(result, token, "editMessageText", chat_id, message_id)
  end

  def handle_reply(token, chat_id, chat_instance, q_id, answer_id, message_id) do
    m = Contest.reply_answer(chat_id, chat_instance, q_id, answer_id)
    send_result(m, token, "editMessageText", chat_id, message_id)
  end

  def user_is_active(chat_id) do
    user = Repo.get_by!(User, %{chat_id: chat_id})
    user.activate
  end
end
