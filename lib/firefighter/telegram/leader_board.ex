defmodule Firefighter.Telegram.LeaderBoard do
  import Ecto.Query

  alias Firefighter.{Repo, User}
  alias Firefighter.Telegram.API

  def get_top_user(chat_id, limit \\ 5) do
    q = from(u in User,
             where: u.point > 0,
             order_by: [desc: :point],
             select: %{first_name: u.first_name, point: u.point, chat_id: u.chat_id},
             limit: ^limit)
    top_users = Repo.all(q)
    case top_users do
      [] -> %{"text": "در حال حاضر هیچ کسی در مسابقه شرکت نکرده", "reply_markup": API.back_to_menu()}
      users ->
        res = Enum.with_index(users, 1)
        |> Enum.reduce("         🏆🥇🥈🥉\r\n", fn(x, acc) ->
          acc <> \
          if elem(x, 0).chat_id == chat_id do
            "👉 <b>#{elem(x, 1)} - #{elem(x, 0).first_name} (#{elem(x, 0).point} امتیاز)</b>" \
          else
            "  #{elem(x, 1)} - #{elem(x, 0).first_name} (#{elem(x, 0).point} ﺎﻤﺗیﺍﺯ)" \
          end
          <> "\r\n"
        end)
        %{"text": res, "parse_mode": "HTML", "reply_markup": API.back_to_menu()}
    end
  end
end
