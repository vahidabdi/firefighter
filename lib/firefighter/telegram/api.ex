defmodule Firefighter.Telegram.API do


  def back_to_menu_keyboard do
    [
      [
        %{"text": "بازگشت به منوی اصلی", "callback_data": "back_to_menu"}
      ]
    ]
  end
  def back_to_menu(list) when is_list(list) do
    %{"inline_keyboard": list}
  end
  def back_to_menu do
    [
      [
        %{"text": "بازگشت به منوی اصلی", "callback_data": "back_to_menu"}
      ]
    ]
    |> back_to_menu()
  end

  def build_main_menu(list) when is_list(list) do
    %{"inline_keyboard": list}
  end
  def build_main_menu do
    [
      [
        %{"text": "😂 👈 مسابقه جایزه بزرگ یاور آتش نشان 👉 😊", "callback_data": "contest"}
      ],
      [
        %{"text": "🤝 مسابقه دعوت از دوستان با کسب امتیاز 👥", "callback_data": "invite"}
      ],
      [
        %{"text": "مسابقه خاطره با جایزه نیم سکه طلا", "callback_data": "contest_mem"}
      ],
      [
        %{"text": "⛑️ آموزش امداد و نکات ایمنی در آتش سوزی و حوادث ⛑️", "callback_data": "4soori_content"}
      ],
      [
        %{"text": "🔥 تاریخچه جشن چهارشنبه سوری 🔥", "callback_data": "history_content"}
      ],
      [
        %{"text": "🏆 تابلو امتیازات 🎁", "callback_data": "award"}
      ],
      [
        %{"text": "😞 لغو عضویت 👋", "callback_data": "unsub"}
      ]
    ]
    |> build_main_menu
  end

  def build_activate_menu(list) when is_list(list) do
    %{"inline_keyboard": list}
  end
  def build_activate_menu do
    [
      [
        %{"text": "تایید", "callback_data": "activate"}
      ]
    ]
    |> build_activate_menu
  end

  def build_question(chat_id, question, q_id, answers) do
    Enum.map(answers,
      fn(x) -> [%{"text": x.answer, "callback_data": "reply_#{q_id}_#{x.id}"}]
    end)
    |> build_question(question)
  end

  def build_question(keyboard, question) do
    IO.puts(inspect keyboard)
    %{"text": question, "reply_markup": %{"inline_keyboard": keyboard ++ back_to_menu_keyboard()}}
  end

  def build_reply(answer_reply, point) do
    kb = [[%{"text": "سوال بعدی", "callback_data": "contest"}]]
    reply = """
    امتیاز شما: #{point}
    #{answer_reply}
    """
    %{"text": reply, "reply_markup": %{"inline_keyboard": kb ++ back_to_menu_keyboard()}}
  end

  def build_content_menu(category, content) do
    kb = [
      [
        %{"text": "محتوای بعدی", "callback_data": "next_#{category}_content"},
        %{"text": "محتوای قبلی", "callback_data": "prev_#{category}_content"}
      ]
    ]
    %{"text": content, "reply_markup": %{"inline_keyboard": kb ++ back_to_menu_keyboard()}}
  end
end
