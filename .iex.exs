import Ecto.Query

alias Firefighter.{
  Repo,
  User,
}

alias Firefighter.Telegram.{
  API,
  Content
}
